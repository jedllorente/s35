const express = require('express')
const router = express.Router();
const courseController = require('../Controllers/course-controller')
const auth = require('../auth')

router.post('/', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    if (userData.isAdmin == false) {
        res.send(`Logged in account is not an admin account`)
    } else {
        courseController.addCourse(req.body).then(resultFromController => {
            res.send(resultFromController)
        })
    }

})

/* retrieve all courses */
router.get('/all-courses', (req, res) => {
    courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})


/* retrieve all active courses */
router.get('/active-courses', (req, res) => {
    courseController.getActiveCourses().then(resultFromController => res.send(resultFromController))
})

/* retrieve specific course */
router.get('/:courseId', (req, res) => {
    /* console.log(req.params)
    console.log(req.params.courseId) */
    courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

/* update a course */
router.put('/:courseId', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    if (userData.isAdmin == false) {
        res.send(`User Not Authorized`)
    } else {
        courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
    }

})

/* course archiver */
router.put('/:courseId/archive', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    if (userData.isAdmin == false) {
        res.send(`User not Authorized.`)
    } else {
        courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
    }
})

module.exports = router;