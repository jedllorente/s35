const user = require('../models/User');
const bcrypt = require('bcrypt')
const auth = require('../auth')

/* check_email duplicate */
module.exports.checkEmailExists = (reqBody) => {
    return user.find({ email: reqBody.email }).then(result => {
        if (result.length > 0) {
            return true
        } else {
            return false
        }
    })
}

/* register user account wuth pass encryption */
module.exports.registerUser = (reqBody) => {
    let newUser = new user({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
            // Syntax:
            // bcrypt.hasSync(<stringToBeHashed>, <saltRounds>)
    })
    return newUser.save().then((user, error) => {
        if (error) {
            return false
        } else {
            return true
        }
    })

}

/* user authentication */
module.exports.loginUser = (reqBody) => {
    return user.findOne({ email: reqBody.email }).then(result => {
        if (result == null) {
            return false
        } else {
            /* if bcrypt is present this will only detect encrypted passwords */
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            if (isPasswordCorrect) {
                return { access: auth.createAccessToken(result) }
            } else {
                return false
            }
        }
    })
}

/* get profile method */
module.exports.getProfile = (reqBody) => {
    return user.findOne({ id: reqBody.id }).then(result => {
        console.log(result)
        if (result == null) {
            return false
        } else {
            result.password = ' '
            return result.save().then((success, saveErr) => {
                if (saveErr) {
                    console.log(saveErr)
                    return false
                } else {
                    return success
                }
            })
        }
    })
}